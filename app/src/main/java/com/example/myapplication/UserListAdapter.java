package com.example.myapplication;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class UserListAdapter extends RecyclerView.Adapter<EventListAdapter.MyViewHolder> {
    private List<User> users;

    private final View.OnClickListener mOnClickListener = new UserListAdapter.MyOnClickListener();

    public class MyOnClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(final View view) {

            if (!KeyInfo.getCurrentUser().accountType.equals("parent"))
                return;

            RecyclerView mRecyclerView = EventFragment.getEventFragment().getEventListView();
            int itemPosition = mRecyclerView.getChildLayoutPosition(view);
            User item = users.get(itemPosition);
            changeFragment(item);
            //Toast.makeText(mContext, item, Toast.LENGTH_LONG).show();
        }

        public void changeFragment(User user)
        {
            Log.i("FamilyMember",user.name + " was clicked");
            MainActivity.GetMainActivity().changeFragment(
                    UserDataFragment.newInstance(user.name, user.category,user.databaseKey), "userData");
        }
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout layout;
        public MyViewHolder(LinearLayout x) {
            super(x);
            layout = x;
        }
    }

    public UserListAdapter(List<User> userList) {
        users = userList;
    }

    @Override
    public EventListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        LinearLayout layout = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_on_list, parent, false);

        layout.setOnClickListener(mOnClickListener);
        EventListAdapter.MyViewHolder vh = new EventListAdapter.MyViewHolder(layout);
        return vh;
    }

    @Override
    public void onBindViewHolder(EventListAdapter.MyViewHolder holder, final int position) {
        TextView nameView = holder.layout.findViewById(R.id.user_name);
        TextView typeView = holder.layout.findViewById(R.id.user_type);

        nameView.setText(users.get(position).name);
        typeView.setText(users.get(position).category);
    }



    @Override
    public int getItemCount() {
        return users.size();
    }
}
