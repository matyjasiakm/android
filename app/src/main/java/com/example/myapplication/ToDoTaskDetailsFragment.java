package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ToDoTaskDetailsFragment extends Fragment {


    private OnFragmentInteractionListener mListener;
    private ToDoTask task;

    public ToDoTaskDetailsFragment() {
        // Required empty public constructor
    }

    public static ToDoTaskDetailsFragment newInstance(ToDoTask task) {
        ToDoTaskDetailsFragment fragment = new ToDoTaskDetailsFragment();
        fragment.task = task;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_to_do_task_details, container, false);
        TextView title = (TextView)rootView.findViewById(R.id.title);
        title.setText(task.title);
        TextView localization = (TextView)rootView.findViewById(R.id.localiztaion);
        localization.setText(task.localiztaion);
        TextView description = (TextView)rootView.findViewById(R.id.description);
        description.setText(task.description);

        String hour = toDateString(task.hourFrom) + ":" + toDateString(task.minutesFrom) +
                " - " + toDateString(task.hourTo) + ":" + toDateString(task.minutesTo);
        DataConstructor dataConstructor = new DataConstructor();
        String date = dataConstructor.writeDate(task);


        TextView time = (TextView)rootView.findViewById(R.id.time);
        time.setText(date + " " + hour);
        TextView isDone = (TextView)rootView.findViewById(R.id.isDone);
        isDone.setText(task.isDone?R.string.done:R.string.done);
        Button markDone = (Button)rootView.findViewById(R.id.markDone);
        markDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                task.isDone = true;
                HashMap<String,Object> map = new LinkedHashMap<>();
                map.put(task.taskId, task);
                DatabaseConnection.getInstance().getTaskList(task.ownerId).updateChildren(map);
            }
        });
        return rootView;
    }

    private String toDateString(Integer i)
    {
        return i < 10 ? "0" + i : i.toString();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
    }
}
