package com.example.myapplication;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, EventFragment.OnFragmentInteractionListener,
            CalendarFragment.OnFragmentInteractionListener,MapsFragment.OnFragmentInteractionListener, LocationListener,
            FamilyFragment.OnFragmentInteractionListener, UserDataFragment.OnFragmentInteractionListener,
            ToDoTaskDetailsFragment.OnFragmentInteractionListener
{
    final static int REQUES_CODE_ADD_ELEMENT=1;
    static List<ToDoTask> listOfTasks=new LinkedList<ToDoTask>();
    static List<ChildrenInfo> listOfChildren=new LinkedList<ChildrenInfo>();
    static List<String> childKeysList=new LinkedList<String>();
    static String childKeyId;
    static DatabaseConnection databaseConnection;
    String userName;
    String userMail;
    boolean childConnected;
    DatabaseReference taskListRef;
    DatabaseReference childrenListRef;

    Fragment actualFragment;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;


   private static final String[] INITIAL_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
    };

    @Override
    public void onLocationChanged(Location location) {
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        childrenListRef=databaseConnection.getChildrenList();
        childrenListRef.child(childKeyId).child("hour").setValue(today.hour);
        childrenListRef.child(childKeyId).child("minutes").setValue(today.minute);
        childrenListRef.child(childKeyId).child("year").setValue(today.year);
        childrenListRef.child(childKeyId).child("month").setValue(today.month);
        childrenListRef.child(childKeyId).child("day").setValue(today.monthDay);
        childrenListRef.child(childKeyId).child("latitude").setValue(location.getLatitude());
        childrenListRef.child(childKeyId).child("longatitude").setValue(location.getLongitude());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


    public static class SettingsScreen extends PreferenceFragment
    {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState){
            Log.i("Settings","onClick");
            class Listener implements Preference.OnPreferenceChangeListener{
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    new ReadData().execute();
                    //EventFragment.getEventFragment().getElementsFromDatabase();
                    Log.i("Get data", "read");
                    return true;
                }
            }

            CheckBoxPreference myPref = (CheckBoxPreference) findPreference("show_children_events");
            myPref.setOnPreferenceChangeListener(new Listener());
            CheckBoxPreference myPref2 = (CheckBoxPreference) findPreference("show_adult_events");
            myPref2.setOnPreferenceChangeListener(new Listener());
            CheckBoxPreference myPref3 = (CheckBoxPreference) findPreference("show_past_events");
            myPref3.setOnPreferenceChangeListener(new Listener());
            if (!KeyInfo.getCurrentUser().accountType.equals("parent"))
            {
                PreferenceCategory category = (PreferenceCategory) findPreference("settings_showing");
                category.removePreference(myPref);
                category.removePreference(myPref2);
            }
            return super.onCreateView(inflater,container, savedInstanceState);
        }

        class ReadData extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... params) {
                EventFragment.getEventFragment().getElementsFromDatabase();
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
            }

            @Override
            protected void onPreExecute() {
            }

            @Override
            protected void onProgressUpdate(Void... values) {
            }
        }

    }

    private static MainActivity activity = null;

    public static MainActivity GetMainActivity()
    {
        return activity;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        databaseConnection = DatabaseConnection.getInstance();
        setContentView(R.layout.activity_main);
        childConnected = databaseConnection.isConnectedAsChild();

        if(childConnected)
        {
            childKeyId=databaseConnection.getMyId();

            LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(INITIAL_PERMS,1);
            }
            else if(lm.isProviderEnabled("gps")){

                lm.requestLocationUpdates("gps",0,0,this);
                lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0,this);
                Location location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if(location!=null) {
                    Time today = new Time(Time.getCurrentTimezone());
                    today.setToNow();
                    childrenListRef = databaseConnection.getChildrenList();
                    childrenListRef.child(childKeyId).child("hour").setValue(today.hour);
                    childrenListRef.child(childKeyId).child("minutes").setValue(today.minute);
                    childrenListRef.child(childKeyId).child("year").setValue(today.year);
                    childrenListRef.child(childKeyId).child("month").setValue(today.month);
                    childrenListRef.child(childKeyId).child("day").setValue(today.monthDay);
                    childrenListRef.child(childKeyId).child("latitude").setValue(location.getLatitude());
                    childrenListRef.child(childKeyId).child("longatitude").setValue(location.getLongitude());
                }

                LocationRequest locationRequest = LocationRequest.create();
                locationRequest.setInterval(1000*60*10);
                locationRequest.setFastestInterval(1000*60*2);
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
                fusedLocationClient.getLastLocation()
                        .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                // Got last known location. In some rare situations this can be null.
                                if (location != null) {
                                    Time today = new Time(Time.getCurrentTimezone());
                                    today.setToNow();
                                    childrenListRef = databaseConnection.getChildrenList();
                                    childrenListRef.child(childKeyId).child("hour").setValue(today.hour);
                                    childrenListRef.child(childKeyId).child("minutes").setValue(today.minute);
                                    childrenListRef.child(childKeyId).child("year").setValue(today.year);
                                    childrenListRef.child(childKeyId).child("month").setValue(today.month);
                                    childrenListRef.child(childKeyId).child("day").setValue(today.monthDay);
                                    childrenListRef.child(childKeyId).child("latitude").setValue(location.getLatitude());
                                    childrenListRef.child(childKeyId).child("longatitude").setValue(location.getLongitude());
                                }
                            }
                        });
                locationCallback = new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        if (locationResult.getLastLocation() == null) {
                            return;
                        }

                        Time today = new Time(Time.getCurrentTimezone());
                        today.setToNow();
                        childrenListRef = databaseConnection.getChildrenList();
                        childrenListRef.child(childKeyId).child("hour").setValue(today.hour);
                        childrenListRef.child(childKeyId).child("minutes").setValue(today.minute);
                        childrenListRef.child(childKeyId).child("year").setValue(today.year);
                        childrenListRef.child(childKeyId).child("month").setValue(today.month);
                        childrenListRef.child(childKeyId).child("day").setValue(today.monthDay);
                        childrenListRef.child(childKeyId).child("latitude").setValue(locationResult.getLastLocation().getLatitude());
                        childrenListRef.child(childKeyId).child("longatitude").setValue(locationResult.getLastLocation().getLongitude());
                    }


                };

                fusedLocationClient.requestLocationUpdates(locationRequest,
                        locationCallback,
                        null /* Looper */);



            }


        }
        else
        {

            childrenListRef=databaseConnection.getFamillyDBChild();
            childrenListRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                      childKeysList.clear();
                      for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                          childKeysList.add(dataSnapshot1.getKey().toString());
                      }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            DatabaseReference rf=databaseConnection.getChildrenList();
            rf.addValueEventListener(new ValueEventListener() {
                @TargetApi(Build.VERSION_CODES.N)
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    listOfChildren.clear();
                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                        if(childKeysList.stream().anyMatch(str->str.trim().equals( dataSnapshot1.getKey())))
                            listOfChildren.add(dataSnapshot1.getValue(ChildrenInfo.class));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }
        userMail = getIntent().getStringExtra("Mail");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        taskListRef = databaseConnection.getTaskList();
        taskListRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Map<String,ToDoTask> tasks=(HashMap<String,ToDoTask>)dataSnapshot.getValue();
                if(tasks!=null) {
                    listOfTasks = new LinkedList<ToDoTask>(tasks.values());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent child=new Intent(getApplicationContext(),AddElementActivity.class);
//                child.putExtra("username",KeyInfo.getCurrentUser().name);
//                child.putExtra("databaseKey",KeyInfo.getCurrentUser().databaseKey);
//                //child.putExtra("childList",(Serializable) listOfChildren);
//                startActivityForResult(child,REQUES_CODE_ADD_ELEMENT);
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if(childConnected){
            navigationView.getMenu().findItem(R.id.nav_map).setVisible(false);
        }

        View headerView = navigationView.getHeaderView(0);
        TextView navMail = (TextView) headerView.findViewById(R.id.user_mail);
        navMail.setText(userMail);


        settings = PreferenceManager.getDefaultSharedPreferences(this);

        changeFragment(EventFragment.getEventFragment(),"event_fragment");
    }

    private static SharedPreferences settings;

    public static SharedPreferences getSharedPreference()
    {
        return settings;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==REQUES_CODE_ADD_ELEMENT){
            if(resultCode==Activity.RESULT_OK){
                ToDoTask myObject = (ToDoTask) data.getExtras().getSerializable("Task");
                String key=databaseConnection.getTaskListAll().child(myObject.ownerId).push().getKey();
                myObject.setId(key);
                databaseConnection.getTaskListAll().child(myObject.ownerId).child(key).setValue(myObject);

            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_settings) {
            launchSettingsScreen();
        } else if (id == R.id.nav_calendar) {
            //EventFragment.getEventFragment().getElementsFromDatabase();
            //CalendarFragment.getCalendarFragment().setEvents(new ArrayList<>());
            changeFragment(new CalendarFragment(),"calendarScreen");
        } else if (id == R.id.nav_family) {
            changeFragment(FamilyFragment.getFamilyFragment(),"familyScreen");
        } else if (id == R.id.nav_events) {
            EventFragment.getEventFragment().setUserData(KeyInfo.getCurrentUser().name,KeyInfo.getCurrentUser().databaseKey);
            changeFragment(EventFragment.getEventFragment(),"eventsScreen");
        } else if (id == R.id.nav_map) {

            changeFragment( MapsFragment.newInstance(listOfChildren),"mapFragment");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void changeFragment(Fragment fragment, String tag)
    {
        Log.i("Mainactivity","changeFragmentIn");
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        if (actualFragment != fragment)
        {
            //transaction.add(R.id.displayed_fragment,fragment, tag);
            transaction.replace(R.id.displayed_fragment, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
        else
        {
            actualFragment = fragment;
        }
        Log.i("Mainactivity","changeFragmentOut");

    }

    private void launchSettingsScreen()
    {
        PreferenceFragment settingsScreen = new SettingsScreen();

        changeFragment(settingsScreen,"settingsScreen");

    }
    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
