package com.example.myapplication;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapsFragment extends android.app.Fragment implements OnMapReadyCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    GoogleMap googleMap;
    List<ChildrenInfo> childPositionList;
    TabLayout tabLayout;
    private OnFragmentInteractionListener mListener;

    public MapsFragment() {
        // Required empty public constructor
    }



    public static MapsFragment newInstance(List<ChildrenInfo> param1) {
        MapsFragment fragment = new MapsFragment();

        Bundle args = new Bundle();
        args.putSerializable("List1",(Serializable)param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            childPositionList = (LinkedList<ChildrenInfo>) getArguments().getSerializable("List1");

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_maps,container,false);
        tabLayout=(TabLayout) view.findViewById(R.id.tabl);
        tabLayout.removeAllTabs();
        for(int i=0;i<childPositionList.size();i++)
        {
            tabLayout.addTab(tabLayout.newTab().setText(childPositionList.get(i).name));
        }
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int i=tab.getPosition();
                googleMap.clear();
                LatLng position= new LatLng(childPositionList.get(i).latitude,childPositionList.get(i).longatitude);
                googleMap.addMarker(new MarkerOptions().position(position)
                        .title(childPositionList.get(i).name));
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(position));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        com.google.android.gms.maps.MapFragment mapFragment=(com.google.android.gms.maps.MapFragment) this.getChildFragmentManager().findFragmentById(R.id.mapv2);

        mapFragment.getMapAsync(this);


        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.googleMap=googleMap;
        this.googleMap.setMinZoomPreference(12);
        if(childPositionList.size()!=0) {
            ChildrenInfo ci = childPositionList.get(0);
            LatLng ny = new LatLng(ci.latitude, ci.longatitude);
            this.googleMap.moveCamera(CameraUpdateFactory.newLatLng(ny));
            this.googleMap.addMarker(new MarkerOptions().position(ny).title(ci.name));
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
