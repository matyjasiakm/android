package com.example.myapplication;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.ToDoTask;

import java.util.Calendar;

public class DataConstructor {
    public String writeDate(ToDoTask event)
    {
        Calendar given_day = Calendar.getInstance();
        Integer day = event.day;
        Integer month = event.month;
        Integer year = event.year;
        given_day.set(year, month, day);
        Calendar today =  Calendar.getInstance();
        if (isDatesEqual(given_day, today))
        {
            return MainActivity.GetMainActivity().getResources().getString(R.string.today);
        }
        today.add(Calendar.DATE, 1);
        if (isDatesEqual(given_day, today)) {
            return MainActivity.GetMainActivity().getResources().getString(R.string.tomorrow);
        }
        today.add(Calendar.DATE, -2);
        if (isDatesEqual(given_day, today)) {
            return MainActivity.GetMainActivity().getResources().getString(R.string.yesterday);
        }

        today.add(Calendar.DATE, 1);
        if (today.get(Calendar.WEEK_OF_YEAR) == given_day.get(Calendar.WEEK_OF_YEAR))
        {
            int dayOfWeek = given_day.get(Calendar.DAY_OF_WEEK);
            return getDayOfWeek(dayOfWeek);
        }
        String dayString = day < 10 ? "0" + day : day.toString();
        month = month + 1;
        String monthString = month < 10 ? "0" + month : month.toString();
        String timeStr = dayString + "." + monthString + "." + event.year;
        return timeStr;
    }

    private boolean isDatesEqual(Calendar c1, Calendar c2)
    {
        if (c1.get(Calendar.YEAR) != c2.get(Calendar.YEAR))
            return false;
        if (c1.get(Calendar.MONTH) != c2.get(Calendar.MONTH))
            return false;
        if (c1.get(Calendar.DAY_OF_MONTH) != c2.get(Calendar.DAY_OF_MONTH))
            return false;
        return true;
    }

    private String getDayOfWeek(int day)
    {
        switch (day)
        {
            case Calendar.MONDAY:
                return MainActivity.GetMainActivity().getResources().getString(R.string.monday);
            case Calendar.TUESDAY:
                return MainActivity.GetMainActivity().getResources().getString(R.string.tuesday);
            case Calendar.WEDNESDAY:
                return MainActivity.GetMainActivity().getResources().getString(R.string.wednesday);
            case Calendar.THURSDAY:
                return MainActivity.GetMainActivity().getResources().getString(R.string.thursday);
            case Calendar.FRIDAY:
                return MainActivity.GetMainActivity().getResources().getString(R.string.friday);
            case Calendar.SATURDAY:
                return MainActivity.GetMainActivity().getResources().getString(R.string.saturday);
            case Calendar.SUNDAY:
                return MainActivity.GetMainActivity().getResources().getString(R.string.sunday);
        }
        return "";
    }
}
