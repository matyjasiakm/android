package com.example.myapplication;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ChangeMailDialog extends DialogPreference {

    public ChangeMailDialog(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public ChangeMailDialog(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private EditText passwordView;
    private EditText mailView;

    @Override
    protected void onBindDialogView(View view) {
        passwordView = (EditText) view.findViewById(R.id.changeMailPassword);
        mailView = (EditText) view.findViewById(R.id.emailEdit);

        SharedPreferences pref = getSharedPreferences();

        super.onBindDialogView(view);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (positiveResult) {
            // save shared preferences
        }
    }

    private void init(Context context)
    {
        setPersistent(false);
        setDialogLayoutResource(R.layout.change_mail_dialog);
        setPositiveButtonText(R.string.change);
        setNegativeButtonText(R.string.cancel);
    }

    @Override
    public void onClick(DialogInterface dialog, int which){
        if(which == DialogInterface.BUTTON_POSITIVE) {
            DatabaseConnection connection = DatabaseConnection.getInstance();
            boolean isPasswordCorrect = connection.checkIfPasswordIsCorrect(passwordView.getText().toString());
            if (isPasswordCorrect)
            {
                Log.i("ChangeMail","Pos");
                connection.changeMail(mailView.getText().toString());
                return;
            }

        }

            Log.i("ChangeMail","Neg");
    }
}
