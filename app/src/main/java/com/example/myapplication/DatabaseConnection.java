package com.example.myapplication;

import android.os.Build;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.CheckBox;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class DatabaseConnection {
    private static DatabaseConnection _connection;

    public static DatabaseConnection create(KeyInfo listKey)
    {
        _connection = new DatabaseConnection(listKey);
        return _connection;
    }

    public static DatabaseConnection getInstance()
    {
        return _connection;
    }

    private KeyInfo keyInfo;
    private FirebaseDatabase database;

    private DatabaseConnection(KeyInfo keyInfo)
    {
        this.keyInfo = keyInfo;
        database = FirebaseDatabase.getInstance();
    }

    public boolean isConnectedAsChild()
    {
        if (keyInfo.accountType == null)
            return true;
        return keyInfo.accountType.equals("child");
    }
    public DatabaseReference getTaskListAll()
    {
        return database.getReference("TaskLists");
    }
    public DatabaseReference getTaskList()
    {
        return database.getReference("TaskLists").child(keyInfo.databaseKey);
    }
    public DatabaseReference getTaskList(String databaseKey)
    {
        return database.getReference("TaskLists").child(databaseKey);
    }

    public DatabaseReference getChildrenList()
    {
        return database.getReference("MyChildgren");
    }

    public DatabaseReference getUsers()
    {
        return database.getReference("Users").child(keyInfo.databaseKey);
    }

    public void changePassword(String newPassword)
    {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.getCurrentUser().updatePassword(newPassword);
    }

    public boolean checkIfPasswordIsCorrect(String password)
    {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();
        AuthCredential credential = EmailAuthProvider.getCredential(
                auth.getCurrentUser().getEmail(),
                password
        );

        try {
            user.reauthenticateAndRetrieveData(credential);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
    public String getMyId(){
       return keyInfo.ownerKey;
    }
    public void changeMail(String newMail)
    {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.getCurrentUser().updateEmail(newMail);
    }

    TaskCompletionSource<List<ToDoTask>> tcs = new TaskCompletionSource<>();

    public List<ToDoTask> getAllTasksForFamily(String familyName)
    {
        DatabaseReference ref = database.getReference("Family").child(familyName);
        List<ToDoTask> tasks = new ArrayList<>();
        return null;
    }

    public List<ToDoTask> getAllTasksForChildren(String familyName)
    {
        DatabaseReference ref = database.getReference("Family");
        return null;
    }

    public List<ToDoTask> getAllTasksForUser(boolean if_search_children, boolean if_search_adults)
    {
        List<ToDoTask> tasksToReturn = new ArrayList<>();
        if (if_search_adults || if_search_children) {
            List<String> users = getAllUsersFromFamily(if_search_children, if_search_adults);
            List<ToDoTask> tasks = new ArrayList<>();
            for (String s : users)
                tasks.addAll(getAllTasksForUser(s));
            tasksToReturn = tasks;
        }
        //CheckBox show_children = (CheckBox)findViewById()
        List<ToDoTask> myTasks = getAllTasksForUser(keyInfo.databaseKey);
        for (ToDoTask task: myTasks)
            if (!tasksToReturn.contains(task))
                tasksToReturn.add(task);

        boolean if_show_past = MainActivity.getSharedPreference().getBoolean("show_past_events", false);
        if (if_show_past)
            tasksToReturn.sort(new ComparatorForActualTasks());
        else tasksToReturn.sort(new ComparatorForAllTasks());
        return tasksToReturn;
    }

    public List<ToDoTask> getAllTasksForUser(String userReferenceNr)
    {
        DatabaseReference ref = database.getReference("TaskLists");
        return getAllTasks(ref.child(userReferenceNr));
    }

    private List<ToDoTask> getAllTasks(DatabaseReference reference)
    {
        final List<ToDoTask> tasks = new ArrayList<>();
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("Received","Rec");
                for (DataSnapshot event : dataSnapshot.getChildren()) {
                    Map<String, Object> ev = (HashMap<String, Object>) event.getValue();
                    if (ev != null)
                    {
                        ToDoTask task = ToDoTask.convert(ev);
                        task.taskId = event.getKey();
                        tasks.add(task);
                    }
                }
                tcs.setResult(tasks);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("DatabaseConnection", "loadPost:onCancelled", databaseError.toException());
                tcs.setException(databaseError.toException());
            }
        });

        Task<List<ToDoTask>> t = tcs.getTask();
        try {
            Tasks.await(t);
            Log.i("Received","GetTask");
            if(t.isSuccessful()) {
                t.getResult();
            }
            tcs = new TaskCompletionSource<>();
        } catch (ExecutionException | InterruptedException e) {
            t = Tasks.forException(e);
        }
        return tasks;
    }

    private class ComparatorForActualTasks implements Comparator<ToDoTask>
    {
        @Override
        public int compare(ToDoTask toDoTask, ToDoTask t1) {
            return toDoTask.compareByDateGrowing(t1);
        }
    }

    private class ComparatorForAllTasks implements Comparator<ToDoTask>
    {
        @Override
        public int compare(ToDoTask toDoTask, ToDoTask t1) {
            return toDoTask.compareByDateDecraasing(t1);
        }
    }

    TaskCompletionSource<List<String>> completionFamilies = new TaskCompletionSource<>();

    public List<String> getAllUsersFromFamily(final boolean if_children, final boolean if_adults)
    {
        if (!KeyInfo.getCurrentUser().accountType.equals("parent"))
            return new ArrayList<>();
        DatabaseReference ref = database.getReference("Family").child(KeyInfo.getCurrentUser().family);
        final List<String> familyNames = new ArrayList<>();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("Received families","Rec");
                for (DataSnapshot event : dataSnapshot.getChildren()) {
                    Map<String, Object> ev = (HashMap<String, Object>) event.getValue();
                    if ((event.getKey().toString().equals("Children") && if_children) ||
                            (event.getKey().toString().equals("Parents") && if_adults))
                    {
                        List<String> items = new ArrayList<>();
                        for (Object o : ev.values())
                            items.add((String)o);
                        familyNames.addAll(items);
                    }
                }
                completionFamilies.setResult(familyNames);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("DatabaseConnection", "loadPost:onCancelled", databaseError.toException());
                completionFamilies.setException(databaseError.toException());
            }
        });

        Task<List<String>> t = completionFamilies.getTask();
        try {
            Tasks.await(t);
            Log.i("Received","GetFamily");
            if(t.isSuccessful()) {
                t.getResult();
            }
            completionFamilies = new TaskCompletionSource<>();
        } catch (ExecutionException | InterruptedException e) {
            t = Tasks.forException(e);
        }

        return familyNames;
    }

    public List<ChildrenInfo> getChildgrenInfo(final List<String> keysList){
        final List<ChildrenInfo> temp=new LinkedList<ChildrenInfo>();
        DatabaseReference ref = database.getReference("MyChildgren");
        ref.addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(final DataSnapshot dataSnapshot1:dataSnapshot.getChildren()) {
                    if(keysList.stream().anyMatch(str->str.trim().equals(dataSnapshot1.getKey().toString())))
                        temp.add(dataSnapshot1.getValue(ChildrenInfo.class));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return temp;
    }
    public DatabaseReference getFamillyDBChild(){
        return database.getReference("Family").child(KeyInfo.getCurrentUser().family).child("Children");
    }

    public List<String> getAllUserKeysFromFamily(final boolean if_children, final boolean if_adults)
    {
        DatabaseReference ref = database.getReference("Family").child(KeyInfo.getCurrentUser().family);
        final List<String> familyNames = new ArrayList<>();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("Received families","Rec");
                for (DataSnapshot event : dataSnapshot.getChildren()) {
                    Map<String, Object> ev = (HashMap<String, Object>) event.getValue();
                    if ((event.getKey().toString().equals("Children") && if_children) ||
                            (event.getKey().toString().equals("Parents") && if_adults))
                    {
                        List<String> items = new ArrayList<>();
                        for (Object o : ev.keySet())
                            items.add((String)o);
                        familyNames.addAll(items);
                    }
                }
                completionFamilies.setResult(familyNames);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("DatabaseConnection", "loadPost:onCancelled", databaseError.toException());
                completionFamilies.setException(databaseError.toException());
            }
        });

        Task<List<String>> t = completionFamilies.getTask();
        try {
            Tasks.await(t);
            Log.i("Received","GetFamily");
            if(t.isSuccessful()) {
                t.getResult();
            }
            completionFamilies = new TaskCompletionSource<>();
        } catch (ExecutionException | InterruptedException e) {
            t = Tasks.forException(e);
        }

        return familyNames;
    }

    public List<User> getUserData(List<String> userKeys)
    {
        List<User> list = new ArrayList<>();
        for (String s : userKeys)
        {
            User u = getUserData(s);
            if (u!=null)
                list.add(u);
        }
        return list;
    }

    TaskCompletionSource<List<User>> completionUsers = new TaskCompletionSource<>();

    private User getUserData(String userKey)
    {
        DatabaseReference reference = database.getReference("Users").child(userKey);
        final List<User> users = new ArrayList<User>();

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("Received families","Rec");
                User user = new User();
                for (DataSnapshot event : dataSnapshot.getChildren()) {
                    if (event.getKey().equals("accountType"))
                        user.category = (String)event.getValue();
                    if (event.getKey().equals("name"))
                        user.name = (String)event.getValue();
                    if (event.getKey().equals("databaseKey"))
                        user.databaseKey = (String)event.getValue();
                    //user.name = (String)event.get("name");
                    //user.category = (String)event.get("accountType");
                }
                users.add(user);
                completionUsers.setResult(users);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("DatabaseConnection", "loadPost:onCancelled", databaseError.toException());
                completionUsers.setException(databaseError.toException());
            }
        });

        Task<List<User>> t = completionUsers.getTask();
        try {
            Tasks.await(t);
            Log.i("Received","GetFamily");
            if(t.isSuccessful()) {
               // user = t.getResult();
            }
            completionUsers = new TaskCompletionSource<>();
        } catch (ExecutionException | InterruptedException e) {
            t = Tasks.forException(e);
            return null;
        }

        return users.get(0);
    }

}
