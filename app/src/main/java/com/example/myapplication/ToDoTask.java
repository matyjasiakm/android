package com.example.myapplication;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

public class ToDoTask implements Serializable {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String id;
    String title;
    String localiztaion;
    String description;
    String ownerId;
    boolean isDone;
    int year;
    int month;
    int day;
    int hourFrom;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocaliztaion() {
        return localiztaion;
    }

    public void setLocaliztaion(String localiztaion) {
        this.localiztaion = localiztaion;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHourFrom() {
        return hourFrom;
    }

    public void setHourFrom(int hourFrom) {
        this.hourFrom = hourFrom;
    }

    public int getMinutesFrom() {
        return minutesFrom;
    }

    public void setMinutesFrom(int minutesFrom) {
        this.minutesFrom = minutesFrom;
    }

    public int getHourTo() {
        return hourTo;
    }

    public void setHourTo(int hourTo) {
        this.hourTo = hourTo;
    }

    public int getMinutesTo() {
        return minutesTo;
    }

    public void setMinutesTo(int minutesTo) {
        this.minutesTo = minutesTo;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    int minutesFrom;
    int hourTo;
    int minutesTo;
    String taskId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ToDoTask toDoTask = (ToDoTask) o;
        return year == toDoTask.year &&
                month == toDoTask.month &&
                day == toDoTask.day &&
                hourFrom == toDoTask.hourFrom &&
                minutesFrom == toDoTask.minutesFrom &&
                hourTo == toDoTask.hourTo &&
                minutesTo == toDoTask.minutesTo &&
                Objects.equals(id, toDoTask.id) &&
                Objects.equals(title, toDoTask.title) &&
                Objects.equals(localiztaion, toDoTask.localiztaion) &&
                Objects.equals(description, toDoTask.description) &&
                Objects.equals(ownerId, toDoTask.ownerId) &&
                isDone == toDoTask.isDone;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, localiztaion, description, ownerId, year, month, day, hourFrom, minutesFrom, hourTo, minutesTo);
    }

    public ToDoTask(String title, String ownerId, String localiztaion, String description, int year, int month, int day, int hourFrom, int minutesFrom, int hourTo, int minutesTo) {
        id ="";
        this.title = title;
        this.localiztaion = localiztaion;
        this.description = description;
        this.year = year;
        this.month = month;
        this.day = day;
        this.hourFrom = hourFrom;
        this.minutesFrom = minutesFrom;
        this.hourTo = hourTo;
        this.minutesTo = minutesTo;
        this.ownerId=ownerId;
        this.isDone = false;
    }

    public ToDoTask(String title, String ownerId, String localiztaion, String description, int year, int month, int day, int hourFrom, int minutesFrom, int hourTo, int minutesTo, boolean isDone) {
        id ="";
        this.title = title;
        this.localiztaion = localiztaion;
        this.description = description;
        this.year = year;
        this.month = month;
        this.day = day;
        this.hourFrom = hourFrom;
        this.minutesFrom = minutesFrom;
        this.hourTo = hourTo;
        this.minutesTo = minutesTo;
        this.ownerId=ownerId;
        this.isDone = isDone;
    }

    public static ToDoTask convert(Map<String,Object> map)
    {
        String title = map.get("title").toString();
        String localization = map.get("localiztaion").toString();
        String description = map.get("description").toString();
        String ownerId = map.get("ownerId").toString();
        int year = (int)(long)map.get("year");
        int month = (int)(long)map.get("month");
        int day = (int)(long)map.get("day");
        int hourFrom = (int)(long)map.get("hourFrom");
        int minutesFrom = (int)(long)map.get("minutesFrom");
        int hourTo = (int)(long)map.get("hourTo");
        int minutesTo = (int)(long)map.get("minutesTo");
        boolean isDone = (boolean)map.get("done");
        return new ToDoTask(title,ownerId, localization, description, year, month, day, hourFrom, minutesFrom,
                hourTo, minutesTo, isDone);
    }

    public ToDoTask() {
    }

    public int compareByDateGrowing(ToDoTask t2)
    {
        if (year < t2.year) return -1;
        if (year > t2.year) return 1;
        if (month < t2.month) return -1;
        if (month > t2.month) return 1;
        if (day < t2.day) return -1;
        if (day > t2.day) return 1;
        if (hourFrom < t2.hourFrom) return -1;
        if (hourFrom > t2.hourFrom) return 1;
        if (minutesFrom < t2.minutesFrom) return -1;
        if (minutesFrom > t2.minutesFrom) return 1;
        return 0;
    }

    public int compareByDateDecraasing(ToDoTask t2)
    {
        return -compareByDateGrowing(t2);
    }
}
