package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserDataFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserDataFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserDataFragment extends Fragment {

    // TODO: Rename and change types of parameters
    private String name;
    private String type;
    private String databaseNr;

    private OnFragmentInteractionListener mListener;

    public UserDataFragment() {
        // Required empty public constructor
    }


    public static UserDataFragment newInstance(String par_name, String par_type, String par_databaseNr) {
        UserDataFragment fragment = new UserDataFragment();
        Bundle args = new Bundle();
        args.putString("name", par_name);
        args.putString("type", par_type);
        args.putString("databaseNr", par_databaseNr);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString("name");
            type = getArguments().getString("type");
            databaseNr = getArguments().getString("databaseNr");
        }
    }

    View rootView;

    List<ToDoTask> userEvents;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_user_data, container, false);
        userEvents = new ArrayList<>();
        TextView username = (TextView)rootView.findViewById(R.id.user_name);
        TextView usertype = (TextView)rootView.findViewById(R.id.user_type);
        username.setText(name);
        usertype.setText(type);

        SetListener(R.id.show_events_button, new ShowEventsListener());
        SetListener(R.id.show_calendar_button, new ShowCalendarListener());
        EventGetter getter = new EventGetter(name, databaseNr);
        getter.GetAppriopriateEvents(userEvents);
        return rootView;
    }

    private void SetListener(int buttonId, View.OnClickListener listener)
    {
        LinearLayout button = (LinearLayout) rootView.findViewById(buttonId);
        button.setOnClickListener(listener);
    }

    private class ShowEventsListener implements View.OnClickListener
    {
        @Override
        public void onClick(View view) {
            EventFragment fragment = EventFragment.getEventFragment();
            fragment.setUserData(name,databaseNr);
            MainActivity.GetMainActivity().changeFragment(fragment, "eventFragment");
        }
    }
    private class ShowCalendarListener implements View.OnClickListener
    {
        @Override
        public void onClick(View view) {
            CalendarFragment fragment = CalendarFragment.newInstance(name, databaseNr);
            MainActivity.GetMainActivity().changeFragment(fragment,"calendarFragment");
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            //mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        //void onFragmentInteraction(Uri uri);
    }
}
