package com.example.myapplication;

import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.util.Log;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;

public class EventGetter {
    String actualUser;
    String databaseNr;

    List<ToDoTask> events;

    public EventGetter()
    {
        actualUser = KeyInfo.getCurrentUser().name;
        databaseNr = KeyInfo.getCurrentUser().databaseKey;
    }

    public EventGetter(String user, String databaseNr)
    {
        actualUser = user;
        this.databaseNr = databaseNr;
    }

    public void GetAppriopriateEvents(List<ToDoTask> events)
    {
        boolean if_show_past = MainActivity.getSharedPreference().getBoolean("show_past_events", false);
        if_search_children = MainActivity.getSharedPreference().getBoolean("show_children_events", false);
        if_search_adults = MainActivity.getSharedPreference().getBoolean("show_adult_events", false);
        if (if_show_past)
            GetAllEvents(events);
        else GetAllFutureEvents(events);
    }

    private boolean if_search_children;
    boolean if_search_adults;

    private void setIfSearchParams()
    {
        KeyInfo currentUser= KeyInfo.getCurrentUser();
        if (!currentUser.accountType.equals("parent"))
        {
            if_search_adults = false;
            if_search_children = false;
        }
    }

    private void GetAllEvents(List<ToDoTask> events)
    {
        setIfSearchParams();
        events.clear();
        new ReadOperation(if_search_adults, if_search_children, events).execute();
    }

    private void GetAllFutureEvents(List<ToDoTask> events)
    {
        setIfSearchParams();
        events.clear();
        new ReadFutureEvents(if_search_adults, if_search_children, events).execute();
    }

    private class ReadOperation extends AsyncTask<Void, Void, Void> {
        boolean if_search_adults;
        boolean if_search_children;
        List<ToDoTask> events;

        public ReadOperation(boolean if_search_adults, boolean if_search_children, List<ToDoTask> events) {
            this.if_search_adults = if_search_adults;
            this.if_search_children = if_search_children;
            this.events = events;
        }

        @Override
        protected Void doInBackground(Void... params) {
            DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
            List<ToDoTask> tasks;
            if (KeyInfo.getCurrentUser().name.equals(actualUser))
                tasks = databaseConnection.getAllTasksForUser(if_search_children, if_search_adults);
            else tasks = databaseConnection.getAllTasksForUser(databaseNr);
            for (ToDoTask task : tasks)
                events.add(task);
            Log.i("EventFragment","AddedEvent");
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            EventFragment.getEventFragment().setRecyclerView();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private class ReadFutureEvents extends AsyncTask<Void, Void, Void> {
        boolean if_search_adults;
        boolean if_search_children;
        List<ToDoTask> events;

        public ReadFutureEvents(boolean if_search_adults, boolean if_search_children, List<ToDoTask> events) {
            this.if_search_adults = if_search_adults;
            this.if_search_children = if_search_children;
            this.events = events;
        }

        @Override
        protected Void doInBackground(Void... params) {
            Calendar cal = Calendar.getInstance();
            Date date = cal.getTime();
            DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
            List<ToDoTask> tasks;
            if (KeyInfo.getCurrentUser().name.equals(actualUser))
                tasks = databaseConnection.getAllTasksForUser(if_search_children, if_search_adults);
            else tasks = databaseConnection.getAllTasksForUser(databaseNr);
            for (ToDoTask task : tasks)
            {
                cal.set(task.year, task.month, task.day, task.hourTo, task.minutesTo, 0);
                Date taskDate = cal.getTime();
                if (taskDate.after(date))
                    events.add(task);
            }
            Log.i("EventFragment","AddedEvent");
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            EventFragment.getEventFragment().setRecyclerView();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }
}
