package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;


public class EventFragment extends Fragment {

    final static int REQUES_CODE_ADD_ELEMENT=1;
    private OnFragmentInteractionListener mListener;

    public RecyclerView getEventListView() {
        return eventListView;
    }

    private RecyclerView eventListView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private String actualUser;
    private String actualUserDatabaseKey;

    public List<ToDoTask> getEvents() {
        return events;
    }

    private List<ToDoTask> events = new ArrayList<ToDoTask>();

    private static EventFragment fragment;

    public static EventFragment getEventFragment()
    {
        if (fragment == null)
            fragment = new EventFragment();


        return fragment;
    }

    public void setUserData(String username, String databaseKey)
    {
        actualUser = username;
        actualUserDatabaseKey = databaseKey;
    }

    public EventFragment() {
        actualUser = KeyInfo.getCurrentUser().name;
        actualUserDatabaseKey = KeyInfo.getCurrentUser().databaseKey;
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static EventFragment newInstance() {
        return getEventFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void getElementsFromDatabase()
    {
        events.clear();
        EventGetter getter = new EventGetter(actualUser, actualUserDatabaseKey);
        getter.GetAppriopriateEvents(events);
    }


    public void setRecyclerView(View rootView)
    {
        Log.i("ShoppingFragment","setRecyclerViewIn");
        eventListView = (RecyclerView) rootView.findViewById(R.id.event_list);

        layoutManager = new LinearLayoutManager(getActivity());
        eventListView.setLayoutManager(layoutManager);

        adapter = new EventListAdapter(events);
        eventListView.setAdapter(adapter);
        Log.i("ShoppingFragment","setRecyclerViewOut");
    }

    public void setRecyclerView()
    {
        Log.i("ShoppingFragment","setRecyclerViewIn");
        eventListView = (RecyclerView) rootView.findViewById(R.id.event_list);

        layoutManager = new LinearLayoutManager(getActivity());
        eventListView.setLayoutManager(layoutManager);

        adapter = new EventListAdapter(events);
        eventListView.setAdapter(adapter);
        Log.i("ShoppingFragment","setRecyclerViewOut");
    }

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_event, container, false);
        setRecyclerView(rootView);
        getElementsFromDatabase();

        Button button = (Button) rootView.findViewById(R.id.startAddEvent);
        final Context context = rootView.getContext();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent child=new Intent(context,AddElementActivity.class);
                child.putExtra("username",actualUser);
                child.putExtra("databaseKey",actualUserDatabaseKey);
                startActivityForResult(child,REQUES_CODE_ADD_ELEMENT);
            }
        });

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            //mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        //void onFragmentInteraction(Uri uri);
    }
}
