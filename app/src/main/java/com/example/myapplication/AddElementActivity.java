package com.example.myapplication;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.io.Serializable;
import java.text.DateFormat;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class AddElementActivity extends AppCompatActivity {

    Button saveButton;
    Button cancelButton;
    EditText title;
    EditText description;
    EditText localization;
    String userName;
    String userDatabaseKey;
    Spinner spin;
    static EditText date;
    static EditText timeFrom;
    static EditText timeTo;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_element);

        saveButton=(Button)findViewById(R.id.saveButton);
        cancelButton=(Button)findViewById(R.id.cancleButton);
        title=(EditText)findViewById(R.id.editTitle);

        description=(EditText)findViewById(R.id.descriptionEditText);
        localization=(EditText)findViewById(R.id.editLocation);
        //spin=(Spinner)findViewById(R.id.spin);
        userName = getIntent().getStringExtra("username");
        userDatabaseKey = getIntent().getStringExtra("databaseKey");
        date=(EditText)findViewById(R.id.editDate);
        date.setInputType(InputType.TYPE_NULL);
        TextView textView = (TextView)findViewById(R.id.add_element_username);
        Log.i("username",userName);
        textView.setText(userName);

        date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                view.requestFocus();
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if(MotionEvent.ACTION_UP == motionEvent.getAction()) {
                    ShowDateSetFragment(view);

                }
                return true;
            }
        });
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ShowDateSetFragment(view);
            }
        });
        timeFrom=(EditText)findViewById(R.id.timeFrom);
        timeFrom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (MotionEvent.ACTION_UP == motionEvent.getAction()) {
                    view.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(AddElementActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String hour = selectedHour < 10 ? "0" + Integer.toString(selectedHour) : Integer.toString(selectedHour);
                            String minutes = selectedMinute < 10 ? "0" + Integer.toString(selectedMinute) : Integer.toString(selectedMinute);
                            timeFrom.setText(hour + ":" + minutes);
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle(R.string.selectStartTime);
                    mTimePicker.show();

                }
                return true;
            }
        });
        timeTo=(EditText)findViewById(R.id.timeTo);
        timeTo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (MotionEvent.ACTION_UP == motionEvent.getAction()) {
                    view.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(AddElementActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String hour = selectedHour < 10 ? "0" + Integer.toString(selectedHour) : Integer.toString(selectedHour);
                            String minutes = selectedMinute < 10 ? "0" + Integer.toString(selectedMinute) : Integer.toString(selectedMinute);
                            timeTo.setText(hour + ":" + minutes);
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle(R.string.selectStartTime);
                    mTimePicker.show();

                }
                return true;
            }
        });

    }


    public void SaveButtonFunc(View view){
        String titleS=title.getText().toString();
        String descriptionS=description.getText().toString();
        String localiztaionS=localization.getText().toString();
        String dateS=date.getText().toString();
        String timeFromS=timeFrom.getText().toString();
        String timeToS=timeTo.getText().toString();
        boolean error=false;
        if(titleS.trim().equalsIgnoreCase("")){
            title.setError("Task can not be undefined!");
            error=true;
        }
        if(localiztaionS.trim().equalsIgnoreCase("")){
            localization.setError("Location can not be undefined!");
            error=true;
        }
        if(timeFromS.toString().trim().equalsIgnoreCase(""))
        {
            timeFrom.setError("Start time can not be null!");
            error=true;
        }
        if(timeFromS.toString().trim().equalsIgnoreCase(""))
        {
            timeFrom.setError("Start time can not be null!");
            error=true;
        }
        if(timeToS.toString().trim().equalsIgnoreCase(""))
        {
            timeTo.setError("End time can not be null!");
            error=true;
        }
        if(dateS.toString().trim().equalsIgnoreCase(""))
        {
            date.setError("Date can not be null!");
            error=true;
        }
        if(error==true)return;

        String[] hour_minutesFrom=timeFromS.split(":");
        String[] hour_minutesTo=timeToS.split(":");
        String[] day_tmonth_year=dateS.split("/");
//        int position=spin.getSelectedItemPosition();
        ToDoTask task=new ToDoTask(titleS,userDatabaseKey,localiztaionS,descriptionS,Integer.parseInt(day_tmonth_year[2]),Integer.parseInt(day_tmonth_year[1]),Integer.parseInt(day_tmonth_year[0])
                ,Integer.parseInt(hour_minutesFrom[0]),Integer.parseInt(hour_minutesFrom[1]),Integer.parseInt(hour_minutesTo[0]),Integer.parseInt(hour_minutesTo[1]));
        String keytocurrentReport = DatabaseConnection.getInstance().getTaskList().push().getKey();
        HashMap<String,Object> map = new LinkedHashMap<>();
        map.put(keytocurrentReport, task);
        DatabaseConnection.getInstance().getTaskList(userDatabaseKey).updateChildren(map);
        Intent returnItems = new Intent();
        returnItems.putExtra("Task", (Serializable) task);
        setResult(Activity.RESULT_OK,returnItems);
        finish();
    }

    public void ReturnButtonFunc(){

        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    public static class DateSetFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar=Calendar.getInstance();
            int day=calendar.get(Calendar.DAY_OF_MONTH);
            int month=calendar.get(Calendar.MONTH);
            int year=calendar.get(Calendar.YEAR);
            return new DatePickerDialog(getActivity(),this,year,month,day);
        }

        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            String day=i2<10?"0"+Integer.toString(i2):Integer.toString(i2);
            String month=i1<10?"0"+Integer.toString(i1):Integer.toString(i1);
            date.setText(i2+"/"+i1+"/"+i);
        }
    }

    public void ShowDateSetFragment(View v){
        DialogFragment df=new DateSetFragment();
        df.show(getFragmentManager(),"datePick");

    }


}
