package com.example.myapplication;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class ChangePasswordDialog extends DialogPreference {

    public ChangePasswordDialog(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public ChangePasswordDialog(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    @Override
    protected void onBindDialogView(View view) {
        newPasswordView = (EditText) view.findViewById(R.id.newPassword);
        retypePasswordView = (EditText) view.findViewById(R.id.retypePassword);
        oldPasswordView = (EditText) view.findViewById(R.id.changePasswordPassword);

        SharedPreferences pref = getSharedPreferences();

        super.onBindDialogView(view);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (positiveResult) {
            // save shared preferences
        }
    }

    private void init(Context context)
    {
        setPersistent(false);
        setDialogLayoutResource(R.layout.change_password_dialog);
        setPositiveButtonText(R.string.change);
        setNegativeButtonText(R.string.cancel);
    }

    @Override
    public void onClick(DialogInterface dialog, int which){
        if(which == DialogInterface.BUTTON_POSITIVE) {
            Log.i("ChangePassword","Pos");
            boolean ifPasswordIsGood = checkIfPasswordIsGood();
            if (ifPasswordIsGood)
            {
                DatabaseConnection connection = DatabaseConnection.getInstance();
                connection.changePassword(newPasswordView.getText().toString());
            }
        }else if(which == DialogInterface.BUTTON_NEGATIVE){
            Log.i("ChangePassword","Neg");
        }
    }

    EditText oldPasswordView;
    EditText newPasswordView;
    EditText retypePasswordView;

    private boolean checkIfPasswordIsGood()
    {
        String newPassword = newPasswordView.getText().toString();
        String retypePassword = retypePasswordView.getText().toString();
        if (!newPassword.equals(retypePassword))
            return false;
        DatabaseConnection connection = DatabaseConnection.getInstance();
        return connection.checkIfPasswordIsCorrect(oldPasswordView.getText().toString());
    }
}
