package com.example.myapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.MyViewHolder>  {
    private List<ToDoTask> events;

    private final View.OnClickListener mOnClickListener = new MyOnClickListener();

    public class MyOnClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(final View view) {
            RecyclerView mRecyclerView = EventFragment.getEventFragment().getEventListView();
            int itemPosition = mRecyclerView.getChildLayoutPosition(view);
            ToDoTask item = events.get(itemPosition);
            ToDoTaskDetailsFragment fragment = ToDoTaskDetailsFragment.newInstance(item);
            MainActivity.GetMainActivity().changeFragment(fragment,"toDoTask");
            //Toast.makeText(mContext, item, Toast.LENGTH_LONG).show();
        }
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout layout;
        public MyViewHolder(LinearLayout x) {
            super(x);
            layout = x;
        }
    }

    public EventListAdapter(List<ToDoTask> eventList) {
        events = eventList;
    }

    @Override
    public EventListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        LinearLayout layout = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_on_list, parent, false);

        layout.setOnClickListener(mOnClickListener);
        MyViewHolder vh = new MyViewHolder(layout);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        TextView titleView = holder.layout.findViewById(R.id.event_title);
        TextView placeView = holder.layout.findViewById(R.id.event_place);
        TextView dateView = holder.layout.findViewById(R.id.date_in_task);
        TextView hourView = holder.layout.findViewById(R.id.hour_in_task);

        titleView.setText(events.get(position).title);
        placeView.setText(events.get(position).localiztaion);
        String hour = toDateString(events.get(position).hourFrom) + ":" + toDateString(events.get(position).minutesFrom) +
                " - " + toDateString(events.get(position).hourTo) + ":" + toDateString(events.get(position).minutesTo);
        hourView.setText(hour);
        DataConstructor dataConstructor = new DataConstructor();
        dateView.setText(dataConstructor.writeDate(events.get(position)));

    }

    private String toDateString(Integer i)
    {
        return i < 10 ? "0" + i : i.toString();
    }



    @Override
    public int getItemCount() {
        TextView view = (TextView) MainActivity.GetMainActivity().findViewById(R.id.emptyListMessage);
        if (events.size() == 0)
            view.setVisibility(View.VISIBLE);
        else view.setVisibility(View.INVISIBLE);
        return events.size();
    }

}
