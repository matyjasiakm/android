package com.example.myapplication;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FamilyFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FamilyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FamilyFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private List<User> users = new ArrayList<>();

    public FamilyFragment() {
        // Required empty public constructor
    }


    public static FamilyFragment newInstance() {
        FamilyFragment fragment = new FamilyFragment();
        return fragment;
    }

    private static FamilyFragment family_fragment = null;

    public static FamilyFragment getFamilyFragment()
    {
        if (family_fragment != null)
            return family_fragment;
        family_fragment = newInstance();
        return family_fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_family, container, false);
        users.clear();
        getData();
        // Inflate the layout for this fragment
        return rootView;
    }

    private class ReadOperation extends AsyncTask<Void, Void, Void> {

        public ReadOperation() {
        }

        @Override
        protected Void doInBackground(Void... params) {
            DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
            List<String> all_users = databaseConnection.getAllUserKeysFromFamily(true, true);
            users = databaseConnection.getUserData(all_users);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            setRecyclerView(rootView);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private void getData()
    {
        new ReadOperation().execute();
    }

    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView usersListView;

    private void setRecyclerView(View rootView)
    {
        Log.i("FamilyFragment","setRecyclerViewIn");
        usersListView = (RecyclerView) rootView.findViewById(R.id.user_list);

        layoutManager = new LinearLayoutManager(getActivity());
        usersListView.setLayoutManager(layoutManager);

        adapter = new UserListAdapter(users);
        usersListView.setAdapter(adapter);
        Log.i("ShoppingFragment","setRecyclerViewOut");
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
