package com.example.myapplication;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.os.SystemClock;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CalendarFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CalendarFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CalendarFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    String actualUser;
    String databaseKey;

    public static CalendarFragment newInstance(String username, String databaseKey)
    {
        CalendarFragment fragment = new CalendarFragment();
        fragment.actualUser = username;
        fragment.databaseKey = databaseKey;
        return fragment;
    }

    public CalendarFragment() {
        actualUser = KeyInfo.getCurrentUser().name;
        databaseKey = KeyInfo.getCurrentUser().databaseKey;
    }

    static CalendarFragment fragment;

    public static CalendarFragment getCalendarFragment()
    {
        if (fragment == null)
            fragment = new CalendarFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    MaterialCalendarView mCalendarView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_calendar, container, false);
        mCalendarView = (MaterialCalendarView) rootView.findViewById(R.id.calendar_view);
        initializeCalendar();
        return rootView;
    }

    private List<ToDoTask> events = new ArrayList<>();

    private void initializeCalendar()
    {
        Log.i("initializeCalendar","init");
        mCalendarView.clearSelection();
        Calendar calendar = Calendar.getInstance();
        CalendarDay today = CalendarDay.from(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1,
                calendar.get(Calendar.DAY_OF_MONTH));
        List<CalendarDay> actualDate = new ArrayList<CalendarDay>();
        actualDate.add(today);
        mCalendarView.addDecorators(new EventDecorator(Color.parseColor("#00ff00"), actualDate));
        GetAppriopriateEvents(events);

        mCalendarView.setCurrentDate(today);
    }

    boolean if_search_children;
    boolean if_search_adults;

    private void GetAppriopriateEvents(List<ToDoTask> events)
    {
        if_search_children = MainActivity.getSharedPreference().getBoolean("show_children_events", false);
        if_search_adults = MainActivity.getSharedPreference().getBoolean("show_adult_events", false);
        boolean if_show_past = MainActivity.getSharedPreference().getBoolean("show_past_events", false);
        if_search_children = MainActivity.getSharedPreference().getBoolean("show_children_events", false);
        setIfSearchParams();
        if_search_adults = MainActivity.getSharedPreference().getBoolean("show_adult_events", false);
        if (if_show_past)
            GetAllEvents(events);
        else GetAllFutureEvents(events);
    }

    private void setIfSearchParams()
    {
        KeyInfo currentUser= KeyInfo.getCurrentUser();
        if (!currentUser.accountType.equals("parent"))
        {
            if_search_adults = false;
            if_search_children = false;
        }
    }

    private void GetAllEvents(List<ToDoTask> events)
    {
        setIfSearchParams();
        events.clear();
        new ReadOperation(if_search_adults, if_search_children, events).execute();
    }

    private void GetAllFutureEvents(List<ToDoTask> events)
    {
        setIfSearchParams();
        events.clear();
        new ReadFutureEvents(if_search_adults, if_search_children, events).execute();
    }

    private class ReadOperation extends AsyncTask<Void, Void, Void> {
        boolean if_search_adults;
        boolean if_search_children;
        List<ToDoTask> events;

        public ReadOperation(boolean if_search_adults, boolean if_search_children, List<ToDoTask> events) {
            this.if_search_adults = if_search_adults;
            this.if_search_children = if_search_children;
            this.events = events;
        }

        @Override
        protected Void doInBackground(Void... params) {
            DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
            List<ToDoTask> tasks;
            if (KeyInfo.getCurrentUser().name.equals(actualUser))
                tasks = databaseConnection.getAllTasksForUser(if_search_children, if_search_adults);
            else tasks = databaseConnection.getAllTasksForUser(databaseKey);
            for (ToDoTask task : tasks)
                events.add(task);
            Log.i("EventFragment","AddedEvent");
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            List<CalendarDay> days = new ArrayList<>();
            for (ToDoTask task: events)
            {
                Calendar calendar = Calendar.getInstance();
                CalendarDay day = CalendarDay.from(task.year, task.month+1, task.day);
                days.add(day);
            }
            mCalendarView.addDecorators(new EventDecorator(Color.parseColor("#ff0000"), days));
            //EventFragment.getEventFragment().setRecyclerView();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private class ReadFutureEvents extends AsyncTask<Void, Void, Void> {
        boolean if_search_adults;
        boolean if_search_children;
        List<ToDoTask> events;

        public ReadFutureEvents(boolean if_search_adults, boolean if_search_children, List<ToDoTask> events) {
            this.if_search_adults = if_search_adults;
            this.if_search_children = if_search_children;
            this.events = events;
        }

        @Override
        protected Void doInBackground(Void... params) {
            Calendar cal = Calendar.getInstance();
            Date date = cal.getTime();
            DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
            List<ToDoTask> tasks;
            if (KeyInfo.getCurrentUser().name.equals(actualUser))
                tasks = databaseConnection.getAllTasksForUser(if_search_children, if_search_adults);
            else tasks = databaseConnection.getAllTasksForUser(databaseKey);
            for (ToDoTask task : tasks)
            {
                cal.set(task.year, task.month, task.day, task.hourTo, task.minutesTo, 0);
                Date taskDate = cal.getTime();
                if (taskDate.after(date))
                    events.add(task);
            }
            Log.i("EventFragment","AddedEvent");
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            List<CalendarDay> days = new ArrayList<>();
            for (ToDoTask task: events)
            {
                Calendar calendar = Calendar.getInstance();
                CalendarDay day = CalendarDay.from(task.year, task.month+1, task.day);
                days.add(day);
            }
            mCalendarView.addDecorators(new EventDecorator(Color.parseColor("#ff0000"), days));
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public void setEvents(List<ToDoTask> events)
    {
        this.events = events;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
        }
    }

    public class EventDecorator implements DayViewDecorator {

        private final int color;
        private final HashSet<CalendarDay> dates;

        public EventDecorator(int color, Collection<CalendarDay> dates) {
            this.color = color;
            this.dates = new HashSet<>(dates);
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return dates.contains(day);
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.addSpan(new BackgroundColorSpan(color));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
    }
}
