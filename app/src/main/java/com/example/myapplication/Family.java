package com.example.myapplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Family {
    public String name;

    public Map<String,String> Parents;

    public Map<String,String> Children;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getParents() {
        return Parents;
    }

    public void setParents(Map<String, String> parents) {
        Parents = parents;
    }

    public Map<String, String> getChildren() {
        return Children;
    }

    public void setChildren(Map<String, String> children) {
        Children = children;
    }

    public Family()
    {
        Parents = new HashMap<String,String>();
        Children = new HashMap<String,String>();
    }

    public Family(String name, String parent, String key)
    {
        Parents = new HashMap<String,String>();
        Children = new HashMap<String,String>();
        this.name = name;
        Parents.put(parent,key);
    }
}
