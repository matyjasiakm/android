package com.example.myapplication;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

public class RegisterActivity extends AppCompatActivity {

    static EditText email;
    static EditText passwd;
    static EditText passwd2;
    static EditText name;
    Button signUp;
    static RadioButton child;
    static RadioButton parent;
    static CheckBox is_family_created;
    static EditText familyName;
    private FirebaseAuth auth;
    FirebaseDatabase database;
    DatabaseReference myRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        email=(EditText)findViewById(R.id.emailEditR);
        passwd=(EditText)findViewById(R.id.passwordEditR);
        passwd2=(EditText)findViewById(R.id.passwordEditR2);
        signUp=(Button)findViewById(R.id.buttonRegister);
        child=(RadioButton)findViewById(R.id.childCheckBox);
        parent=(RadioButton)findViewById(R.id.parentCheckBox);
        name=(EditText)findViewById(R.id.nameEditR) ;
        is_family_created=(CheckBox)findViewById(R.id.if_create_family);
        familyName=(EditText)findViewById(R.id.family_name);

        database = FirebaseDatabase.getInstance();

        child.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(true==b){
                    is_family_created.setVisibility(View.GONE);
                    is_family_created.setChecked(false);
                    myRef=null;
                }
                else
                {
                    is_family_created.setVisibility(View.VISIBLE);
                    myRef=null;
                }
            }
        });
        parent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(true!=b){
                    is_family_created.setVisibility(View.GONE);
                    is_family_created.setChecked(false);
                    myRef=null;
                }
                else
                {
                    is_family_created.setVisibility(View.VISIBLE);
                    myRef=null;

                }
            }
        });
        auth = FirebaseAuth.getInstance();
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                final String emailS=email.getText().toString().trim();
                final String passwdS=passwd.getText().toString().trim();
                String passwd2S=passwd2.getText().toString().trim();
                final boolean ifCreateFamily = is_family_created.isSelected();
                final String family_name = familyName.getText().toString();
                if(TextUtils.isEmpty(emailS)){

                    email.setError("Email can not be empty!");
                    return;
                }
                if (TextUtils.isEmpty(passwdS)) {
                    passwd.setError("Password can not be empty!");
                }
                if (TextUtils.isEmpty(passwd2S)) {
                    passwd2.setError("Password can not be empty!");
                }
                if (!passwd2S.equals(passwd2S)) {
                    Toast.makeText(getApplicationContext(), R.string.diffPasswords, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (passwd.length() < 6) {
                    Toast.makeText(getApplicationContext(), R.string.passwordToShort, Toast.LENGTH_SHORT).show();
                    return;
                }

                //important to do
//                if (searchFamilyName(family_name) == ifCreateFamily)
//                {
//                    if (ifCreateFamily)
//                        Toast.makeText(getApplicationContext(), "Such family exists", Toast.LENGTH_SHORT).show();
//                    else
//                        Toast.makeText(getApplicationContext(), "Such family does not exists", Toast.LENGTH_SHORT).show();
//                    return;
//                }



                //if(child.isChecked()){
//                    if(TextUtils.isEmpty(childKeyS)){
//                        keyChild.setError("Key can not be empty!");
//                        return;
//                    }
//                    class ChceckIfParentExist implements ValueEventListener{
//
//                        @Override
//                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                            if(dataSnapshot.hasChild(childKeyS)){
//
//                                Register();
//
//                            }
//                            else
//                            {
//                            keyChild.setError("Parent does not exist!");
//                            }
//                        }
//
//                        @Override
//                        public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                        }
//
//
//                        void Register(){
//
//                            auth.createUserWithEmailAndPassword(emailS, passwdS)
//                                    .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
//                                        @Override
//                                        public void onComplete(@NonNull Task<AuthResult> task) {
//
//
//                                            // If sign in fails, display a message to the user. If sign in succeeds
//                                            // the auth state listener will be notified and logic to handle the
//                                            // signed in user can be handled in the listener.
//                                            if (!task.isSuccessful()) {
//                                                Toast.makeText(RegisterActivity.this, "Authentication failed." + task.getException(),
//                                                        Toast.LENGTH_SHORT).show();
//                                            } else {
//
//                                                //family.child(family_name).child("Users").setValue(task.getResult().getUser().getUid().toString());
//
//                                                DatabaseReference childRef=database.getReference("Users").child(task.getResult().getUser().getUid().toString());
//                                                DatabaseReference childListref=database.getReference("Family").child("Children").child(childKeyS);
//                                                childListref.push().setValue(new ChildrenInfo(task.getResult().getUser().getUid().toString(),name.getText().toString()));
//
//                                                childRef.setValue(new KeyInfo(task.getResult().getUser().getUid().toString(),childKeyS,"child",name.getText().toString()));
//                                                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
//
//                                                finish();
//                                            }
//                                        }
//                                    });
//                        }
//                    }
//                    myRef=database.getReference("Family").child("OwnerDBList");
//                    myRef.addValueEventListener(new ChceckIfParentExist());
               // }
                //else


                    auth.createUserWithEmailAndPassword(emailS, passwdS)
                            .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {


                                    // If sign in fails, display a message to the user. If sign in succeeds
                                    // the auth state listener will be notified and logic to handle the
                                    // signed in user can be handled in the listener.
                                    if (!task.isSuccessful()) {
                                        Toast.makeText(RegisterActivity.this, R.string.authFailed,
                                                Toast.LENGTH_SHORT).show();
                                    } else {

                                        myRef=database.getReference("Users").child(task.getResult().getUser().getUid().toString());
                                        DatabaseReference dbRef=database.getReference("TaksLists");
                                        String key=dbRef.push().getKey().toString();

                                        if (ifCreateFamily)
                                        {
                                            DatabaseReference family = database.getReference("Family").child(family_name);
                                            Family f = new Family(family_name, task.getResult().getUser().getUid().toString(), key);
                                            family.setValue(f);
                                        }
                                        else {
                                            DatabaseReference ref = database.getReference("Family").child(family_name);
                                            if (parent.isChecked())
                                                ref = ref.child("Parents");
                                            else {
                                                ref = ref.child("Children");
                                                DatabaseReference rf=database.getReference("MyChildgren").child(task.getResult().getUser().getUid().toString());
                                                rf.setValue(new ChildrenInfo(task.getResult().getUser().getUid().toString(),name.getText().toString(),key));
                                            }
                                            Map<String, Object> map = new HashMap<>();
                                            map.put(task.getResult().getUser().getUid().toString(),key);
                                            ref.updateChildren(map);

                                        }

                                        String type = "child";
                                        if (parent.isChecked())
                                            type = "parent";

                                        //database.getReference("OwnerDBList").child(key).setValue(new KeyInfo(task.getResult().getUser().getUid().toString(),key,"parent",name.getText().toString()));
                                        myRef.setValue(new KeyInfo(task.getResult().getUser().getUid().toString(),key,type,name.getText().toString(),family_name));

                                        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                        finish();
                                    }
                                }
                            });

                //}





            }
        });


    }

    private boolean searchFamilyName(String familyName)
    {
        List<String> allFamilies = getAllFamilies();

        for (int i=0;i<allFamilies.size();i++)
            if (allFamilies.get(i).equals(familyName))
                return true;
        return false;

    }

    TaskCompletionSource<List<String>> completionFamilies = new TaskCompletionSource<>();

    public List<String> getAllFamilies()
    {
        final CountDownLatch done = new CountDownLatch(1);
        DatabaseReference ref = database.getReference("Family");
        final List<String> familyNames = new ArrayList<>();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)  {
                Log.i("Received families","Rec");
                for (DataSnapshot event : dataSnapshot.getChildren()) {
                    Map<String, Object> ev = (HashMap<String, Object>) event.getValue();
                    if (ev != null)
                    {
                        for (String s: (String[])ev.keySet().toArray())
                            familyNames. add(s);
                    }
                }
                completionFamilies.setResult(familyNames);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("DatabaseConnection", "loadPost:onCancelled", databaseError.toException());
                completionFamilies.setException(databaseError.toException());
            }
        });

        Task<List<String>> nam = completionFamilies.getTask();
        try {
            Tasks.await(nam);
            Log.i("Received","GetTask");
            if(nam.isSuccessful()) {
                nam.getResult();
            }
            completionFamilies = new TaskCompletionSource<List<String>>();
        } catch (ExecutionException | InterruptedException e) {
            nam = Tasks.forException(e);
        }
        return familyNames;
    }

}
