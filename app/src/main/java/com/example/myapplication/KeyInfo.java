package com.example.myapplication;

public class KeyInfo {
    String ownerKey;
    String databaseKey;
    String accountType;
    String name;
    String family;
    public String getOwnerKey() {
        return ownerKey;
    }

    public void setOwnerKey(String ownerKey) {
        this.ownerKey = ownerKey;
    }

    public String getDatabaseKey() {
        return databaseKey;
    }

    public void setDatabaseKey(String databaseKey) {
        this.databaseKey = databaseKey;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }


    public KeyInfo(String owner,String database,String type,String name, String family){
        ownerKey=owner;
        databaseKey=database;
        accountType=type;
        this.name=name;
        this.family = family;
    }

    public KeyInfo() {
    }

    static KeyInfo current_user;

    public static void setCurrentUser(KeyInfo keyInfo)
    {
        current_user = keyInfo;
    }

    public static  KeyInfo getCurrentUser()
    {
        return current_user;
    }
}
