package com.example.myapplication;

import java.io.Serializable;

public class ChildrenInfo implements Serializable {

    String key;
    String db_key;
    String name;
    double latitude;
    double longatitude;
    int year;
    int month;
    int day;
    int hour;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDb_key() {
        return db_key;
    }

    public void setDb_key(String db_key) {
        this.db_key = db_key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongatitude() {
        return longatitude;
    }

    public void setLongatitude(double longatitude) {
        this.longatitude = longatitude;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    int minutes;

    public ChildrenInfo(String key, String name,String db_key,int year,int month,int day ,int hour,int minutes,double latitude,double longatitude) {
        this.name = name;
        this.key = key;
        this.year=year;
        this.month=month;
        this.day=day;
        this.hour=hour;
        this.minutes=minutes;
        this.latitude=latitude;
        this.longatitude=longatitude;
        this.db_key=db_key;
    }
    public ChildrenInfo(String key, String name,String db_key) {
        this.name = name;
        this.key = key;
        this.db_key=db_key;
        this.year=-1;
        this.month=-1;
        this.day=-1;
        this.hour=-1;
        this.minutes=-1;
        this.latitude=-1;
        this.longatitude=-1;
    }

    public ChildrenInfo() {
    }
}
